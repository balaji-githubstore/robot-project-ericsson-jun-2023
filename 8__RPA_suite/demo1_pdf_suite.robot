*** Settings ***
Library     RPA.PDF

*** Test Cases ***
TC1
    ${page_count}   Get Number Of Pages     source_path=C:${/}AutomationConcepts${/}profile.pdf
    Log To Console    ${page_count}
    ${text}     Get Text From Pdf   source_path=C:${/}AutomationConcepts${/}profile.pdf
    Log   ${text}
    ${page_1}   Convert To Number    1
    Log    ${text}[${page_1}]
    Should Contain    ${text}[${page_1}]    PDF to WORD

TC2
    Open Pdf    source_path=C:${/}AutomationConcepts${/}profile.pdf
    ${page_count}   Get Number Of Pages
    Log To Console    ${page_count}
    ${text}     Get Text From Pdf
    Log   ${text}
    ${page_1}   Convert To Number    1
    Log    ${text}[${page_1}]
    Should Contain    ${text}[${page_1}]    PDF to WORD
