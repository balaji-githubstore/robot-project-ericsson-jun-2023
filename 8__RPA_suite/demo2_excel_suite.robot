*** Settings ***
Library     RPA.Excel.Files

*** Tasks ***
TC1
    Open Workbook    path=${EXECDIR}${/}test_data${/}open_emr_data.xlsx
    @{sheets}   List Worksheets
    ${sheet_count}      Get Length    ${sheets}
    Log    ${sheet_count}
    Log    ${sheets}
    FOR    ${sheet_name}    IN    @{sheets}
        Log    ${sheet_name}
    END

TC2 Read
    Open Workbook    path=${EXECDIR}${/}test_data${/}open_emr_data.xlsx
    ${sheet}      Read Worksheet      VerifyInvalidLoginTemplate
    Log    ${sheet}

    Log    ${sheet}[0]
    Log    ${sheet}[0][A]
    Log    ${sheet}[0][B]
    Log    ${sheet}[1][A]

    ${row_count}      Get Length    ${sheet}
    Log    ${row_count}
    ${col_count}      Get Length    ${sheet}[0]
    Log    ${col_count}

TC3 Read and Write
    Open Workbook    path=${EXECDIR}${/}test_data${/}open_emr_data.xlsx
    ${sheet}      Read Worksheet      VerifyInvalidLoginTemplate
    ${cell}     Get Worksheet Value    1    1
    Log    ${cell}
    Set Worksheet Value    2    4    hello
    Save Workbook
    Close Workbook
