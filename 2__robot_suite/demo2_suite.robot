*** Settings ***
Library    String

*** Variables ***
${BROWSER_NAME}     chrome
${MOBILE_NUMBER}     458877

*** Test Cases ***
TC1
    ${num1}     Set Variable    45
    ${result}   Evaluate    ${num1}+${num1}
    Set Global Variable    ${num1}      ${result}
    Log To Console    ${num1}
    Log To Console    ${BROWSER_NAME}
    Log To Console    Please launch ${BROWSER_NAME} for ${num1} times

TC2
    Log To Console    ${BROWSER_NAME}
    Log To Console    ${num1}

TC3
    ${output}   Convert To Upper Case   ${BROWSER_NAME}
    Log To Console    ${output}

TC4
    ${sal1}     Set Variable    $123,101
    ${sal2}     Set Variable    $123,000,001
    ${sal1}     Remove String    ${sal1}    $   ,
    Log To Console   ${sal1}
    ${sal2}     Remove String    ${sal2}    $   ,
    #print the sum of two salary
    ${res}   Evaluate    ${sal1}+${sal2}
    Log To Console    ${res}
