
*** Keywords ***
Calculate Area Of Cirle
    [Arguments]     ${radius}
    ${result}   Evaluate     3.14*${radius}*${radius}
    [Return]    ${result}

*** Test Cases ***
TC1
    ${output}   Calculate Area Of Cirle    5
    Log To Console    ${output}
    ${output}   Calculate Area Of Cirle    10
    Log To Console    ${output}

