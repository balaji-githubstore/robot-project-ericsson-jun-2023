
*** Test Cases ***
TC1
    ${num}      Set Variable      0
    IF    ${num}<0
        Log To Console    The ${num} number is negative
    ELSE IF   ${num}==0
        Log To Console    It's zero
    ELSE
        Log To Console    The ${num} number is positive
    END

TC2
    #1 to 10
    FOR    ${i}    IN RANGE    0    3
        Log To Console    ${i}
    END

TC3
    @{fruits}   Create List   mango     apple      grapes   kiwi
    ${size}     Get Length    ${fruits}
    FOR    ${i}    IN RANGE    0    ${size}
        Log To Console    ${fruits}[${i}]
    END