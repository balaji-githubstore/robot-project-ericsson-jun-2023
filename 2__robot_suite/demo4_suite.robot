*** Settings ***
Library    Collections
*** Variables ***
&{caps}     deviceName=redmi    platformName=android    platformVersion=12.0
@{COLORS}  red  yellow  green

*** Test Cases ***
TC1
    Log To Console    ${caps}
    Log To Console    ${caps}[platformName]

TC2
    #create a dictionary locally - add empid=101, empname=john, salary=7777
    &{dic}   Create Dictionary   empId=100   empName=jack    salary=7777.2     dress_codes=${COLORS}
    Log Dictionary  ${dic}
    Log    Name: ${dic}[empName], salary: ${dic}[salary]
    Log To Console    ${dic}
    Log To Console    ${dic}[dress_codes]
    Log To Console    ${dic}[dress_codes][0]