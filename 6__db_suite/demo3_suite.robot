*** Settings ***
Library     DatabaseLibrary

Suite Setup      Connect To Database  dbapiModuleName=pymysql     dbName=dbfree_db
   ...  dbUsername=dbfree_db    dbPassword=12345678     dbHost=db4free.net
   ...  dbPort=3306

Suite Teardown   Disconnect From Database

*** Comments ***
--MySql, postgre, oracle, mssql
Table Name - Products
Column Name - Product_ID, ProductName, description

*** Test Cases ***
TC1 Get Row Count
   ${row_count}      DatabaseLibrary.Row Count    select * from Products
   Log To Console    ${row_count}


TC2 Table Description
   ${desc}  Description    select * from Products   
   Log To Console    ${desc}

TC3
    Row Count Is Equal To X    select * from Products    156
    Row Count Is Greater Than X    select * from Products    100
    Row Count Is Less Than X    select * from Products    200
#    Row Count Is Equal To X    select * from Products where product_name=''    1

TC4
    ${result}   Query    select * from Products where Product_ID<5
    Log    ${result}
    Log To Console    ${result}
    Log To Console    ${result}[0]
    Log To Console    ${result}[0][0]
    Log To Console    ${result}[0][1]
    Log To Console    ${result}[0][2]
    Log To Console    1 Product ID: ${result}[0][0] 1 Product Name: ${result}[0][1], 2 Product ID: ${result}[1][0] 2 Product Name: ${result}[1][1]
    Log    1 Product ID: ${result}[0][0] 1 Product Name: ${result}[0][1], 2 Product ID: ${result}[1][0] 2 Product Name: ${result}[1][1]

TC5 Update
    Execute Sql String    update Products set description='digital watch' where product_ID=1
    Row Count Is Equal To X    select * from Products where product_ID=1 and description='digital watch'    1
    ${result}   Query    select description from Products where Product_ID=1
    Log    ${result}
    Log To Console    ${result}[0][0]

TC6 Insert and verify it
    Execute Sql String    Insert into Products (Product_ID, ProductName, description) values (88889,'Balaji','trainer')
    Row Count Is Equal To X    select * from Products where product_ID=888    1
    Check If Exists In Database    select * from Products where product_ID=888
#888,Balaji,trainer
#Please insert the record to the table

TC8
    ${result}   Query    select * from Products
    Log    ${result}
    Log To Console    ${result}
    Log To Console    ${result}[0]
    Log To Console    row1: ${result}[0] ${SPACE} row2: ${result}[1]

    FOR    ${i}    IN RANGE    0    156
        Log To Console    productid: ${result}[${i}][0]
    END

TC9 Delete 
    Execute Sql String  Delete from Products where product_ID=888
    Row Count Is Equal To X    select * from Products where product_ID=888    0
    Check If Not Exists In Database    select * from Products where product_ID=888

#TC10
#    DatabaseLibrary.Execute Sql Script    scriptfile.sql