*** Settings ***
Library     RequestsLibrary


*** Test Cases ***
TC1 Find Valid Pet Id
    Create Session    alias=petstore    url=https://petstore.swagger.io/v2
    ${response}  GET On Session      alias=petstore  url=/pet/5    expected_status=200
#    Status Should Be    expected_status=200
    Log    ${response.status_code}
    Log    ${response.json()}
    Log    ${response.json()}[id]
    Log    ${response.json()}[name]
    Should Be Equal As Strings    ${response.json()}[name]    doggie
    Should Be Equal As Numbers    ${response.json()}[id]    5
#will start at 2:05 PM IST
#TC2
#    ${cert_path}  Set Variable  /path/to/cert.pem
#    ${key_path}  Set Variable  /path/to/key.pem
#
#    @{client certs}=  create list   ${cert_path}    ${key_path}
#
#    &{headers_dic}      Create Dictionary       Authorization=Bearer AUIIUDF8989778ds
#
#    Create Client Cert Session  alias=cert  url=${url}  client_certs=${client certs}  headers=${headers_dic}
#    ...   verify=${False}
#    ${response}     GET On Session  alias=cert  url=
#    Log    ${response}


