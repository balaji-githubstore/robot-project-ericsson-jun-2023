*** Settings ***
Library     RequestsLibrary

Suite Setup    Create Custom Session For Ericsson

*** Keywords ***
Create Custom Session For Ericsson
    Create Session    alias=petstore    url=https://petstore.swagger.io/v2

*** Test Cases ***
TC1 Find Valid Pet Id
    ${response}  GET On Session      alias=petstore  url=/pet/5    expected_status=200
    Should Be Equal As Numbers    ${response.json()}[id]    5
    Should Be Equal As Strings    ${response.json()}[name]    doggie

TC2 Find InValid Pet Id
    ${response}   GET On Session      alias=petstore  url=/pet/58998    expected_status=404
    Status Should Be    404
    Should Be Equal As Strings    ${response.json()}[message]    Pet not found

TC3 Find Pet By Valid Status
   ${response}    GET On Session  alias=petstore      url=/pet/findByStatus?status=pending   expected_status=200
    Log   ${response.json()}[0][status]

    ${pet_count}     Get Length    ${response.json()}
    Log    ${pet_count}

    FOR    ${i}    IN RANGE    0     ${pet_count}
        Log   ${response.json()}[${i}][status]
        Should Be Equal As Strings      ${response.json()}[${i}][status]    Pending     ignore_case=True
    END