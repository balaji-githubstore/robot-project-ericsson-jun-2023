XPath

Absolute xpath (not recommended) - /html/body/div[4]/div[1]/div/div[6]/div[1]/div[2]/div/div/div[2]/div/div/div/form/div[5]/div/div/div[1]

Relative xpath
//tagname[@attribute='']
//tagname[text()='']


//a[normalize-space()='Acknowledgments, Licensing and Certification']

Grouping of xpath - starts at 1
(//div[@class='checkbox-ui'])[2]

Contains
//tagname[contains(@attribute,'')]
//tagname[contains(text(),'')]

And-Or

And xpath
//tagname[@attribute='' and @attribute='']
//input[@type='file' and @id='edit-field-additional-document-0-upload']

parent-child

child xpath


ancestor-descendant


following-preceding




Example: validate in chrome --> ctrl+f
//div[@class='checkbox-ui']
//button[@type='submit']
//button[text()='start my free trial']
//button[contains(text(),'free trial')]
//input[contains(@id,'UserFirst')]


//b[contains(text(),'phpMy')]

(//div[@role='alert'])[3]

//a[@class='newclose']

//a[text()='select your product type']

//img[@alt='Go']
//img[contains(@src,'go.gif')]

Check for 

//input[@type='file']
(//input[@type='file'])[2]


Row Count
//table[@id='example']/tbody/tr

---------------------------------------------
CSS Selector 
https://www.w3schools.com/cssref/css_selectors.php

#id
.classname
tagname[attribute='']
[attribute='']

Example:
#citiCard1
input[name='citiCard3']


//select[@data-handler='selectYear']

#date
input[name='date']
[name='date']

Javascript
document.querySelector('#date')

document.querySelector('#date').click()
document.querySelector('#date').value='20-10-2023'

document.querySelector('#date').scrollIntoView()

document.querySelector("body > downloads-manager").shadowRoot.querySelector("#frb0").shadowRoot.querySelector("#show")




