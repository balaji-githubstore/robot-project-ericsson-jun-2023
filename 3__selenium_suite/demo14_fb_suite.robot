*** Settings ***
Library     SeleniumLibrary

Test Setup      Open Browser    url=https://www.facebook.com/   browser=${BROWSER_NAME}
Test Teardown   Run Keywords    Close Browser   AND  Log To Console    end
Test Tags   regression

*** Variables ***
${BROWSER_NAME}     edge

*** Test Cases ***
TC1
    [Tags]      smoke       UI
    ${actual_title}     Get Title
    Log To Console   ${actual_title}
TC2
     [Tags]      smoke       UI
    ${current_url}      Get Location
    Log To Console   ${current_url}
TC3
     [Tags]       UI
    ${page_source}      Get Source  
    Log    ${page_source}
    Sleep    5s
    Close Browser

