*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome
    ...  options=add_argument("start-maximized");add_argument("--disable-notifications");add_argument("--ignore-certificate-errors")
    Maximize Browser Window
    Set Selenium Implicit Wait    10s
    Go To    url=https://www.irctc.co.in/nget/train-search
    ${title}    Get Title
    Log To Console    ${title}
    Title Should Be    IRCTC Next Generation eTicketing System

TC2
    &{prefs}    Create Dictionary       download.default_directory=C:${/}AutomationConcepts
    Open Browser    browser=chrome  options=add_experimental_option("prefs",${prefs})
    Maximize Browser Window
    Set Selenium Implicit Wait    10s
    Go To    url=https://www.selenium.dev/downloads/
    Click Element    partial link=32 bit
    Sleep    15s
    [Teardown]  Close Browser

TC3
    &{dic}    Create Dictionary       deviceName=Nexus 5
    Open Browser    browser=chrome  options=add_experimental_option("mobileEmulation",${dic})
    Maximize Browser Window
    Set Selenium Implicit Wait    10s
    Go To    url=https://www.selenium.dev/downloads/
    Click Element    partial link=32 bit
    Sleep    15s
    [Teardown]  Close Browser