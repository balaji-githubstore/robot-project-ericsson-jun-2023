*** Settings ***
Library    SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html

    ${name1}     Get Text    xpath=//table[@id='example']/tbody/tr[1]/td[2]
    Log To Console    ${name1}

TC2 All Names
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html

    FOR    ${i}    IN RANGE    1    11
        ${name1}     Get Text    xpath=//table[@id='example']/tbody/tr[${i}]/td[2]
        Log To Console    ${name1}
    END

TC3 Click Checkbox For Brenden Wagner
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html

    FOR    ${i}    IN RANGE    1    11
        ${name}     Get Text    xpath=//table[@id='example']/tbody/tr[${i}]/td[2]
        Log To Console    ${name}
        IF    '${name}'=='Brenden Wagner'
            Click Element    xpath=//table[@id='example']/tbody/tr[${i}]/td[1]
            BREAK
        END
    END

TC4 Click Checkbox For Br
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html

    FOR    ${i}    IN RANGE    1    11
        ${name}     Get Text    xpath=//table[@id='example']/tbody/tr[${i}]/td[2]
        Log To Console    ${name}
        #any name that contains 'Br'
        IF   'Br' in '${name}'
            Click Element    xpath=//table[@id='example']/tbody/tr[${i}]/td[1]
        END
    END

TC5 Names from all pages
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html

    FOR    ${page}    IN RANGE    1    7
        ${row_count}     Get Element Count    xpath=//table[@id='example']/tbody/tr
        FOR    ${i}    IN RANGE    1    ${row_count}+1
            ${name1}     Get Text    xpath=//table[@id='example']/tbody/tr[${i}]/td[2]
            Log    ${name1}
        END
        Click Element    id=example_next
    END

TC5 Checkbox any page
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html
    ${found}    Set Variable   False
    FOR    ${page}    IN RANGE    1    7
        ${row_count}     Get Element Count    xpath=//table[@id='example']/tbody/tr
        FOR    ${i}    IN RANGE    1    ${row_count}+1
            ${name}     Get Text    xpath=//table[@id='example']/tbody/tr[${i}]/td[2]
            Log    ${name}
            IF    '${name}'=='Jonas Alexander'
                ${found}    Set Variable   True
                Click Element    xpath=//table[@id='example']/tbody/tr[${i}]/td[1]
                BREAK
            END
        END
        Exit For Loop If    '${found}'=='True'
        Click Element    id=example_next
    END