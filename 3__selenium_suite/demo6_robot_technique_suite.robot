*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1 Forgot UserID Check
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    10s
    Go To    url=https://www.online.citibank.co.in/
    #Close if any pop up comes otherwise ignore
    #check for first pop and if count>0 then click
    ${count}    Get Element Count    xpath=//a[@class='newclose']
    IF    ${count}>0
         Click Element    xpath=//a[@class='newclose']
    END

    #check for second pop and if count>0 then click
    ${count}    Get Element Count    xpath=//a[@class='newclose2']
    IF    ${count}>0
         Click Element    xpath=//a[@class='newclose2']
    END

     Click Element    xpath=//span[text()='Login']


TC2 Forgot UserID Check
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    10s
    Go To    url=https://www.online.citibank.co.in/
    #Close if any pop up comes otherwise ignore
    #check for first pop and if count>0 then click
    Run Keyword And Ignore Error    Click Element    xpath=//a[@class='newclose']
    #check for second pop and if count>0 then click
    Run Keyword And Ignore Error    Click Element    xpath=//a[@class='newclose2']
    Click Element    xpath=//span[text()='Login']

TC3 Forgot UserID Check
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    10s
    Go To    url=https://www.online.citibank.co.in/
    #Close if any pop up comes otherwise ignore
    #check for first pop and if count>0 then click
    ${output}    Run Keyword And Ignore Error    
    ...  Click Element    xpath=//a[@class='newclose12354']
    
    Log    ${output}
    Log To Console    ${output}
    Log    ${output}[0]
    Log    ${output}[1]

#    runs below code until the click get pass
    WHILE    '${output}[0]'=='FAIL'
         ${output}    Run Keyword And Ignore Error
          ...  Click Element    xpath=//a[@class='newclose12354']
    END