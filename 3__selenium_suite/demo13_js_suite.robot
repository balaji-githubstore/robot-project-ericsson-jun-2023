*** Settings ***
Library    SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://phptravels.net/tours
    Execute Javascript      document.querySelector('#date').value='20-10-2023'
    Sleep    2s
    Close Browser

TC2 Citbank With JS
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.citibank.co.in/ssjsps/forgetuseridmidssi.jsp
    Click Element    link=select your product type
    Click Element    link=Credit Card
    Scroll Element Into View    css=#bill-date-long
    Execute Javascript      document.querySelector('#bill-date-long').value='20/09/2010'
    Sleep    2s
    Close Browser

TC3 Citbank JS with WebElement
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.citibank.co.in/ssjsps/forgetuseridmidssi.jsp
    Click Element    link=select your product type
    Click Element    link=Credit Card
    Scroll Element Into View    css=#bill-date-long
    ${ele}      Get WebElement    xpath=//input[@name='DOB']
    Execute Javascript  arguments[0].value='20/09/2010'   ARGUMENTS   ${ele}
    Sleep    2s
    Close Browser
TC5 JS returns
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.citibank.co.in/ssjsps/forgetuseridmidssi.jsp
    ${title}     Execute Javascript      return document.title
    Log To Console    ${title}

TC6 Shadow Element
    &{prefs}    Create Dictionary       download.default_directory=C:${/}AutomationConcepts
    Open Browser    browser=chrome  options=add_experimental_option("prefs",${prefs})
    Maximize Browser Window
    Set Selenium Implicit Wait    10s
    Go To    url=https://www.selenium.dev/downloads/
    Click Element    partial link=32 bit
    Go To    url=chrome://downloads/
#    Element Text Should Be    id=show    Show in folder
    ${result}    Execute Javascript      return document.querySelector("body > downloads-manager").shadowRoot.querySelector("#frb0").shadowRoot.querySelector("#show").innerText
    Log To Console    ${result}


TC7 Shadow Element
    &{prefs}    Create Dictionary       download.default_directory=C:${/}AutomationConcepts
    Open Browser    browser=chrome  options=add_experimental_option("prefs",${prefs})
    Maximize Browser Window
    Set Selenium Implicit Wait    10s
    Go To    url=https://www.selenium.dev/downloads/
    Click Element    partial link=32 bit
    Go To    url=chrome://downloads/
    ${result}    Execute Javascript      return document.querySelector("body > downloads-manager").shadowRoot.querySelector("#frb0").shadowRoot.querySelector("#show").innerText
    Log To Console    ${result}
    ${result}    Execute Javascript      return document.querySelector("body > downloads-manager").shadowRoot.querySelector("#frb0").shadowRoot.querySelector("#show").title
    Execute Javascript  document.querySelector("body > downloads-manager").shadowRoot.querySelector("#frb0").shadowRoot.querySelector("#show").click()
    Log To Console    ${result}