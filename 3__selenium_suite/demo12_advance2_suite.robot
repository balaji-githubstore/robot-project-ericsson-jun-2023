*** Settings ***
Library    SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome  alias=b1
    Open Browser    browser=edge    alias=b2
    Switch Browser    b1
    Go To    url=http://google.com
    Switch Browser    b2
    Go To    url=http://facebook.com
    Sleep    5s
    Close All Browsers

TC2
    Open Browser    browser=chrome  alias=b1
    Go To    url=http://google.com
    #not recommended
    Set Selenium Speed    1s
    Press Keys      None       hello
    Press Keys      None       ARROW_DOWN
    Press Keys      None       ARROW_DOWN
    Press Keys      None       ARROW_DOWN 
    Press Keys      None       ENTER
    Sleep    5s
    Close All Browsers