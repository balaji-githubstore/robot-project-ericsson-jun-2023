*** Settings ***
Library     SSHLibrary

*** Test Cases ***
TC1
    Enable Ssh Logging      ${EXECDIR}${/}test_data${/}sshlog.log
    Open Connection    host=192.168.29.215  port=22     alias=m1
    Login   username=Balaji_Dinakaran   password=123456

    @{dirs}     List Directories In Directory   C:\\mine
    Log  ${dirs}

TC2
    Open Connection    host=192.168.29.215  port=22     alias=m1
    Login With Public Key   username=Balaji_Dinakaran   keyfile=
    Enable Ssh Logging      ${EXECDIR}${/}test_data${/}sshlog.log
    @{dirs}     List Directories In Directory   C:\\mine
    Log  ${dirs}

TC3
    Open Connection    host=192.168.29.215  port=22     alias=m1
    Login   username=Balaji_Dinakaran   password=123456
    ${output}   Execute Command    git --version
    Log    ${output}

TC4
    Open Connection    host=192.168.29.215  port=22     alias=m1
    Open Connection    host=192.168.29.215  port=22     alias=m2

    Login   username=Balaji_Dinakaran   password=123456
    Switch Connection    m1
    Login   username=Balaji_Dinakaran   password=123456
    Close Connection
    Close All Connections

TC4
    Open Connection    host=192.168.29.215  port=22     alias=m1
    Login   username=Balaji_Dinakaran   password=123456
    ${output}   Execute Command    java -jar C:${/}software${/}jenkins.war
    Log    ${output}

TC3
    Open Connection    host=192.168.29.215  port=22     alias=m1
    Login   username=Balaji_Dinakaran   password=123456
    ${output}   Execute Command    appium -p 8989
    Log    ${output}